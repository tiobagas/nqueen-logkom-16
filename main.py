from enum import Enum
from backend import minisatSolver
import tkinter as tk
from tkinter import ttk
from scrollableFrame import ScrollableFrame


# class ScrollableFrame(ttk.Frame):
#     
#     def __init__(self, container, N, *args, **kwargs):
#         super().__init__(container, *args, **kwargs)
#         canvas = tk.Canvas(self)
#         width, height = self.calculateFrameSize(N)
#         canvas.config(width=width, height=height)
#         scrollbar = ttk.Scrollbar(self, orient="vertical", command=canvas.yview)
#         scrollbar_x = ttk.Scrollbar(self, orient="horizontal", command=canvas.xview)
#         self.scrollable_frame = ttk.Frame(canvas)
# 
#         self.scrollable_frame.bind(
#             "<Configure>",
#             lambda e: canvas.configure(
#                 scrollregion=canvas.bbox("all")
#             )
#         )
# 
#         canvas.create_window((0, 0), window=self.scrollable_frame, anchor="nw")
# 
#         scrollbar_x.pack(side="bottom", fill="x")
#         scrollbar.pack(side="right", fill="y")
# 
#         canvas.configure(yscrollcommand=scrollbar.set, xscrollcommand=scrollbar_x.set)
# 
#         canvas.pack(side="left", fill="both", expand=True)
#     def calculateFrameSize(self,N):
#         gap = 2
#         size_mult = 36
#         additional_y = 30
#         if(N <= 17):
#             x,y = (N * (size_mult+gap) , (N * (size_mult+gap)) + additional_y)
#             return x,y
#         else:
#             return 680, 680 + additional_y

class ButtonState(Enum):
    CLICKED = 1
    NOT_CLICKED = 2
    UNDEFINED = 3

def initializeTkinterBoard():
    master = tk.Tk()
    master.title("N Queens Problem")

    colour1 = 'gray'
    colour2 = 'orange'
    dummyImg = tk.PhotoImage()
    queenPhoto = tk.PhotoImage(file='queenImg.png')
    queenIco = tk.PhotoImage(file='queenIco.png')
    master.tk.call('wm', 'iconphoto', master._w, queenIco)
    return master, colour1, colour2, dummyImg, queenPhoto, queenIco

def addLabelToWindow(master, label_name, grid):
    row, column = grid
    newLabel = tk.Label(master, text=label_name) 
    newLabel.grid(column=column, row=row)
    return newLabel

def addEntryToWindow(master, width, grid):
    row, column = grid
    newEntry = tk.Entry(master, width=width)
    newEntry.grid(column=column, row=row)
    return newEntry

def addForm(master, label_name, row):
    newLabel = addLabelToWindow(master, label_name, (row, 0))
    newEntry = addEntryToWindow(master, 20, (row, 1))
    return (newLabel, newEntry)

def makeDummyImage():
    img = tk.PhotoImage()
    return img

def makeImage(filename):
    img = tk.PhotoImage(file=filename)
    return img

def evaluateImageFromState(buttonState, images):
    dummyImg, queenImg = images
    if(buttonState == ButtonState.CLICKED):
        return queenImg
    else:
        return dummyImg

def addButton(master):
    newButton = tk.Button(master) 
    return newButton

def makeNewButton(master, button, buttonState, images):
    bgcolor = button.cget("bg")
    width = button.cget("width")
    height = button.cget("height")
    img = evaluateImageFromState(buttonState, images)
    newButton = tk.Button(master, bg=bgcolor, image=img, width=width, height=height, compound="center") 
    return newButton

def setCommandInButton(button, func):
    button.config(command=func)
    return button

def setCommandInGridButton(master, button, buttonState, images, buttonIndex, listButtons):
    button = setCommandInButton(button, lambda master=master, button=button, buttonIndex=buttonIndex, listButtons=listButtons: handleChessGridButton(master,button, buttonState, images, buttonIndex, listButtons))
    return button

def putButtonInGrid(oldButton, newButton):
    buttonGrid = oldButton.grid_info()
    row, column = (buttonGrid["row"], buttonGrid["column"])
    newButton.grid(row=row, column=column)
    return newButton

def changeStateButton(buttonState):
    if(buttonState == ButtonState.NOT_CLICKED):
        return ButtonState.CLICKED
    elif(buttonState == ButtonState.CLICKED):
        return ButtonState.NOT_CLICKED
    else:
        return ButtonState.UNDEFINED

def setListOfButtons(buttonState, buttonIndex, listButtons):
    if(buttonState == ButtonState.NOT_CLICKED):
        listButtons.remove(buttonIndex)
    else:
        listButtons.append(buttonIndex)
    return listButtons

def handleChessGridButton(master, button, buttonState, images, buttonIndex, listButtons):
    newState = changeStateButton(buttonState)
    listButtons = setListOfButtons(newState, buttonIndex, listButtons)
    newButton = makeNewButton(master, button, newState, images)
    newButton = setCommandInGridButton(master, newButton, newState, images, buttonIndex, listButtons)
    newButton = putButtonInGrid(button, newButton)
    button.destroy()
    return newButton

def clone(widget):
    parent = widget.nametowidget(widget.winfo_parent())
    cls = widget.__class__

    clone = cls(parent)
    for key in widget.configure():
        clone.configure({key: widget.cget(key)})
    return clone

def handleResetChessButton(master):
    master.destroy()
    makeUserChessSizePrompt()

def configureResetChessButton(button, grid):
    row, column = grid
    button.config(text="reset")
    button.config(compound="center")
    button.grid(row=row, column=0, columnspan=column)
    return button

def makeResetButton(master, container, grid):
    resetButton = addButton(container)
    configuredButton = configureResetChessButton(resetButton, grid)
    setCommandInButton(configuredButton, lambda master=master: handleResetChessButton(master))
    return master, container, configuredButton


def configureSubmitChessSizeButton(button, row):
    button.config(text="submit")
    button.grid(row=row, column=2)
    return button

def configureSubmitQueenButton(button, row, N):
    button.config(text="submit")
    button.config(compound="center")
    button.grid(row=row, column=0, columnspan=N)
    return button

def handleSubmitSizeButton(master, entry):
    try: 
        intValue = int(entry.get())
        master.destroy()
        makeChessBoard(intValue)
    except Exception as e:
        print(str(e))
    return


def makeUserChessSizePrompt():
    master = tk.Tk()
    (chessLabel, chessEntry) = addForm(master, "Enter chess size", 0)
    submitButton = addButton(master)
    submitButton = configureSubmitChessSizeButton(submitButton, 0)
    submitButton = setCommandInButton(submitButton, lambda master=master, entry=chessEntry: handleSubmitSizeButton(master, entry))
    master.mainloop()

def handleQueenSubmitButton(master, queens, N):
    master.destroy()
    minisatSolver(N, queens)
    return

def getScreenMiddleFromFrame(frame):
    x0, y0 = (frame.winfo_screenwidth()/2, frame.winfo_screenheight()/2)
    return x0, y0

def setupMainFrame(master):
    mainFrame = tk.Frame(master)
    mainFrame.pack(expand=True, fill=tk.BOTH)
    canvas = tk.Canvas(mainFrame)
    scrollbar = tk.Scrollbar(mainFrame, orient=tk.VERTICAL)
    scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
    scrollbar.config(command=canvas.yview)
    canvas.config(yscrollcommand=scrollbar.set)
    canvas.pack(side=tk.LEFT, expand=True, fill=tk.BOTH)
    chessFrame = tk.Frame(canvas)
   # x0, y0 = getScreenMiddleFromFrame(mainFrame)
    canvas.create_window((0,0), window=chessFrame, anchor="nw")
    return master, chessFrame
    

def drawChessBoard(master, frame, N, colour1, colour2, dummyImg, queenImg):
    colour = colour1
    size = 32 

    r = -1
    c = 0
    listSelectedButtons = []

    for i in range(N*N):

        if (i%N) == 0:
            r += 1
            c = 0

            if (N%2) == 0:
                    colour = colour1 if colour == colour2 else colour2

        currGrid = tk.Button(frame, bg=colour, image=dummyImg, width=size, height=size, compound='center')
        currGrid = setCommandInGridButton(frame, currGrid, ButtonState.NOT_CLICKED, (dummyImg, queenImg), i, listSelectedButtons)
#        currGrid = setCommandInButton(currGrid, lambda master=master, button=currGrid, buttonState=ButtonState.NOT_CLICKED, images=(dummyImg, queenImg), buttonIndex=i, listButtons=listSelectedButtons: handleChessGridButton(master, button, buttonState, images, buttonIndex, listButtons))

        currGrid.grid(row=r, column=c)
        c += 1
        colour = colour1 if colour == colour2 else colour2

    submitButton = addButton(frame)
    submitButton = configureSubmitQueenButton(submitButton, N, N)
    submitButton = setCommandInButton(submitButton, lambda master=master, queens=listSelectedButtons, N=N: handleQueenSubmitButton(master, queens, N))    
    master.mainloop()

def makeChessBoard(N):
    if(N <= 3):
        minisatSolver(N, {})

    else:
        master, colour1, colour2, dummyImg, queenPhoto, queenIco = initializeTkinterBoard()
        # master, chessFrame = setupMainFrame(master)
        scrollableFrame = ScrollableFrame(master, N, 38, True)
        scrollableFrame.pack()
        drawChessBoard(master, scrollableFrame.scrollable_frame,  N, colour1, colour2, dummyImg, queenPhoto)

    return

if __name__ == "__main__":
    makeUserChessSizePrompt()


