# Logika Komputasional - N-Queen Problem
N-Queen Problem dalam Boolean Satisfiablity Problem

# Kelompok 16
- Muhammad Hanif Pratama - 1606876065
- Muhammad Volyando Belvadra - 1606918326
- Tio Bagas Sulistiyanto - 1606918452

# System Requirements
1. Python 3
2. Minisat Solver

# How to run
1. Jalankan file main.py dengan perintah 'python main.py' atau 'python3 main.py'
2. Masukkan ukuran papan N-Queen
3. Pilih tile yang menurut anda merupakan jawaban dari papan N-Queen tersebut (tahap ini dapat dilewati)
4. Klik tombol submit
5. Jika terdapat solusi dari papan (SATISFIABLE), maka akan muncul windows baru yang menampilkan solusi tersebut.