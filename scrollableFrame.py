import tkinter as tk
from tkinter import ttk


class ScrollableFrame(ttk.Frame):
    size_mult = 0
    additional_flag = False
    def __init__(self, container, N, size_mult, additional_flag, *args, **kwargs):
        super().__init__(container, *args, **kwargs)
        canvas = tk.Canvas(self)
        self.size_mult = size_mult
        self.additional_flag = additional_flag
        width, height = self.calculateFrameSize(N)
        canvas.config(width=width, height=height)
        scrollbar = ttk.Scrollbar(self, orient="vertical", command=canvas.yview)
        scrollbar_x = ttk.Scrollbar(self, orient="horizontal", command=canvas.xview)
        self.scrollable_frame = ttk.Frame(canvas)

        self.scrollable_frame.bind(
            "<Configure>",
            lambda e: canvas.configure(
                scrollregion=canvas.bbox("all")
            )
        )

        canvas.create_window((0, 0), window=self.scrollable_frame, anchor="nw")

        scrollbar_x.pack(side="bottom", fill="x")
        scrollbar.pack(side="right", fill="y")

        canvas.configure(yscrollcommand=scrollbar.set, xscrollcommand=scrollbar_x.set)

        canvas.pack(side="left", fill="both", expand=True)

    def calculateFrameSize(self,N):
        additional_y = 30
        x, y = (0,0)
        if(N <= 20):
            x,y = (N * (self.size_mult) , (N * (self.size_mult)))
        else:
            x,y = 680, 680
            # x,y = (N * (self.size_mult) , (N * (self.size_mult)))
        if(self.additional_flag == True):
            y += additional_y
        return x, y
