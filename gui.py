import tkinter as tk
from scrollableFrame import ScrollableFrame
# import main
# from main import makeResetButton

def addLabelToWindow(master, label_name, grid):
    row, column = grid
    newLabel = tk.Label(master, text=label_name) 
    newLabel.grid(column=column, row=row)
    return newLabel

def displayGUI(N):
	from main import makeResetButton

	master, colour1, colour2, dummyImg, queenPhoto, queenIco = initializeTkinter()
	colour = colour1

	with open('clauses.sol', mode='r') as f:
		content = f.read().splitlines()
	f.close()

	solved = content[0]

	if solved=='UNSAT':
		label = addLabelToWindow(master, "Unsatisfiable", (0,0))
		label.config(compound="center")
		label.grid(columnspan=1)
		master, holder, button = makeResetButton(master, master, (1,1))
		# print("This problem is unsolvable for N = %d" %N)
		# master.destroy()
		return
		
	result = content[1].split()

	r = -1
	c = 0
	scrollableFrame = ScrollableFrame(master, N, 34, True)
	scrollableFrame.pack()

	for i in range(N*N):

		if (i%N) == 0:
			r += 1
			c = 0

			if (N%2) == 0:
				colour = colour1 if colour == colour2 else colour2

		tile = int(result[i])

		if tile < 0:
			tk.Label(scrollableFrame.scrollable_frame, bg=colour, image=dummyImg, width=32, height=32, compound='center').grid(row=r,column=c)
		else:
			tk.Label(scrollableFrame.scrollable_frame, bg=colour, image=queenPhoto, width=32, height=32, compound='center').grid(row=r,column=c)

		c += 1
		colour = colour1 if colour == colour2 else colour2
	  
	master, holder, button = makeResetButton(master, scrollableFrame.scrollable_frame, (N, N))

	master.mainloop()

def displayNonGUI(N):

	with open('clauses.sol', mode='r') as f:
		content = f.read().splitlines()
	f.close()

	solved = content[0]

	if solved=='UNSAT':
		print("This problem is unsolvable for N = %d" %N)
		return

	result = content[1].split()

	r = -1
	c = 0

	for i in range(N*N):

		if (i%N) == 0:
			r += 1
			c = 0
			print('')

		tile = int(result[i])

		if tile < 0:
			print(" _ ", end="")
		else:
			print(" X ", end="")

		c += 1

	print('\n')

def initializeTkinter():
    
    master = tk.Tk()
    master.title("N Queens Problem")

    colour1 = 'gray'
    colour2 = 'orange'


    dummyImg = tk.PhotoImage()
    queenPhoto = tk.PhotoImage(file='queenImg.png')
    queenIco = tk.PhotoImage(file='queenIco.png')
    master.tk.call('wm', 'iconphoto', master._w, queenIco)
    return master, colour1, colour2, dummyImg, queenPhoto, queenIco

