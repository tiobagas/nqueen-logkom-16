from math import ceil
from gui import displayGUI, displayNonGUI
import sys
import subprocess


def openFile():
    file = open("clauses.cnf", mode='w')
    return file

file = openFile()

# Main function untuk memecahkan masalah N-Queen Problem
def minisatSolver(N, queenList):
	# global file
	file = openFile()

	totalVariables = N * N
	totalClauses = int(((N * (N - 1) * (5 * N - 1)) / 3) + N) + len(queenList)

	file.write("p cnf {0:d} {1:d}\n".format(totalVariables, totalClauses))

	mainRules(N, file)
	rowRules(N, file)
	columnRules(N, file)
	diagonalRules(N, file)
	printQueens(queenList, file)

	file.close()

	subprocess.call(['minisat', 'clauses.cnf', 'clauses.sol'])

	displayGUI(N)
# 	if N <= 20:
# 		displayGUI(N)
# 	else:
# 		displayNonGUI(N)
# Mencetak gambar queen pada board
def printQueens(listQueen, file):
#    global file
    for i in listQueen:
        file.write("{0:d} 0\n".format(i + 1))
    return

# Fungsi rules akan mencetak clause pada file .cnf
# Nilai positif = True pada CNF
# Nilai negatif = False pada CNF
# Nilai 0 = end of clause

# Main Rules mencetak clause or semua variable pada tiap baris board
# ex 4x4 :  1  2  3  4 0
#           5  6  7  8 0
#           9 10 11 12 0
#          13 14 15 16 0
def mainRules(N, file):
	lim = N * N + 1

	for i in range(1, lim):
		file.write("{0:d} ".format(i))
		if i % N == 0:
			file.write("0\n")

# Row rules mencetak clause kondisi dimana queen tidak boleh
# diletakan pada baris board berdasarkan queen yang ada sebelumnya
# ex 4x4 : -1 -2 0
# artinya 1 -> -2 (jika telah diletakkan queen tile 1, maka
# tidak boleh meletakkan queen pada tile 2, posisi 2 di samping 1)
def rowRules(N, file):
	lim = N * N + 1

	for i in range(1, lim):
		row = ceil(i / N)

		for j in range(i, row * N + 1):
			if j == i:
				continue
			file.write("-{0:d} -{1:d} 0\n".format(i, j))

# Column rules mencetak clause kondisi dimana queen tidak boleh
# diletakan pada kolom board berdasarkan queen yang ada sebelumnya
# ex 4x4 : -1 -5 0
# artinya 1 -> -5 (jika telah diletakkan queen tile 1, maka
# tidak boleh meletakkan queen pada tile 5, posisi 5 di bawah 1)
def columnRules(N, file):
	lim = N * N + 1

	for i in range(1, lim):

		for j in range(i, lim, N):
			if j == i:
				continue
			file.write("-{0:d} -{1:d} 0\n".format(i, j))

# Diagonal rules mencetak clause kondisi dimana queen tidak boleh
# diletakan pada diagonal board berdasarkan queen yang ada sebelumnya
# ex 4x4 : -1 -6 0
# artinya 1 -> -6 (jika telah diletakkan queen tile 1, maka
# tidak boleh meletakkan queen pada tile 6, posisi 6 diagonal kanan dari 1)
def diagonalRules(N, file):
	lim = N * N + 1

	if(N == 1):
		return

	for i in range(1, lim):
		row = ceil(i / N)
		col = i % N

		if col == 0: col = N

		# Clause diagonal kanan
		for j in range(i, min(((N - col + row) * N + 1), lim), N + 1):
			if j == i:
				continue
			file.write("-{0:d} -{1:d} 0\n".format(i, j))
		# Clause diagonal kiri
		for j in range(i, lim, N - 1):
			if j == i:
				continue
			elif ceil((j - (N - 1)) / N) == ceil(j / N):
				break
			file.write("-{0:d} -{1:d} 0\n".format(i, j))
